package org.id4me.repository;

import org.id4me.model.OIDCClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OIDCClientRepository extends JpaRepository<OIDCClient, Long> {
    OIDCClient getOIDCClientByProvider(String provider);

    void deleteAllByProvider(String provider);
}
