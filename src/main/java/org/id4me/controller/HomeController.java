package org.id4me.controller;


import org.id4me.Service.OIDCClientService;
import org.id4me.model.form.RegistrationForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;

@Controller
@SessionAttributes("RegistrationForm")
public class HomeController {

    @Autowired
    OIDCClientService oidcClientService;

    @GetMapping("/")
    String home(Model model) {
        model.addAttribute("RegistrationForm", new RegistrationForm());
        return "home";
    }

    @PostMapping("/registration")
    String registration(@RequestBody @ModelAttribute("RegistrationForm") RegistrationForm registrationForm) throws IOException, ParseException, com.nimbusds.oauth2.sdk.ParseException, URISyntaxException {
        oidcClientService.registerOIDCClient(registrationForm.getProvider(), URI.create(registrationForm.getRedirectUri()));
        return "success";
    }


}
