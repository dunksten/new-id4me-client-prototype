package org.id4me.Service;

import com.nimbusds.jose.util.JSONObjectUtils;
import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.client.ClientRegistrationErrorResponse;
import com.nimbusds.oauth2.sdk.client.ClientRegistrationResponse;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.openid.connect.sdk.op.OIDCProviderMetadata;
import com.nimbusds.openid.connect.sdk.rp.*;
import org.id4me.model.OIDCClient;
import org.id4me.repository.OIDCClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

@Service
public class OIDCClientService {

    private static final Logger logger = LoggerFactory.getLogger(OIDCClientService.class);
    @Autowired
    OIDCClientRepository oidcClientRepository;

    public void registerOIDCClient(String provider, URI redirectUri) throws URISyntaxException, IOException, ParseException, java.text.ParseException {

        logger.info(provider);
        logger.info(redirectUri.toString());

        URI issuerURI = new URI(provider);
        URL providerConfigurationURL = issuerURI.resolve("/c2id/.well-known/openid-configuration").toURL();

        logger.info(providerConfigurationURL.toString());

        InputStream stream = providerConfigurationURL.openStream();
        // Read all data from URL
        String providerInfo;
        try (java.util.Scanner s = new java.util.Scanner(stream)) {
            providerInfo = s.useDelimiter("\\A").hasNext() ? s.next() : "";
        }
        OIDCProviderMetadata providerMetadata = OIDCProviderMetadata.parse(providerInfo);

        //Client registration
        String jsonMetadata = "{\"application_type\": \""
            + (redirectUri.getHost().equals("localhost") ? "native" : "web")
            + "\",\"redirect_uris\": [\"" + redirectUri.toString() + "\"],\"response_types\": [\"code\"]}";
        logger.info("Client registration: " + jsonMetadata);
        OIDCClientMetadata metadata = OIDCClientMetadata.parse(JSONObjectUtils.parse(jsonMetadata));

        // Make registration request
        OIDCClientRegistrationRequest registrationRequest = new OIDCClientRegistrationRequest(providerMetadata.getRegistrationEndpointURI(), metadata, null);
        HTTPResponse regHTTPResponse = registrationRequest.toHTTPRequest().send();

        logger.info("Response: " + regHTTPResponse.getStatusCode() + ", " + regHTTPResponse.getContent());

        // Parse and check response
        ClientRegistrationResponse registrationResponse = OIDCClientRegistrationResponseParser.parse(regHTTPResponse);

        if (registrationResponse instanceof ClientRegistrationErrorResponse) {
            ErrorObject error = ((ClientRegistrationErrorResponse) registrationResponse)
                    .getErrorObject();
            logger.warn(error.getCode(), error.getDescription());
        }

        // Store client information from OP
        OIDCClientInformation clientInformation = ((OIDCClientInformationResponse) registrationResponse).getOIDCClientInformation();
        OIDCClient oidcClient = new OIDCClient();
        oidcClient.setProvider(provider);
        oidcClient.setCliendId(clientInformation.getID());
        oidcClient.setClientSecret(clientInformation.getSecret());
        oidcClient.setRedirectUri(redirectUri);
        oidcClient.setProviderAuthorizationURI(providerMetadata.getAuthorizationEndpointURI());
        oidcClient.setProviderTokenURI(providerMetadata.getTokenEndpointURI());
        oidcClient.setProviderJWKSetURI(providerMetadata.getJWKSetURI());
        oidcClient.setProviderUserInfoURI(providerMetadata.getUserInfoEndpointURI());

        oidcClientRepository.save(oidcClient);

        logger.info("Successful client registration response");
        logger.info("Secret: " + clientInformation.getSecret().getValue());
        logger.info("ID: " + clientInformation.getID().getValue());
    }

    public OIDCClient getOIDCClientfromProvider(String provider) {
        return oidcClientRepository.getOIDCClientByProvider(provider);
    }
}
