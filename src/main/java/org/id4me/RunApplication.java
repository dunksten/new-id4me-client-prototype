package org.id4me;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class RunApplication {

    public static void main(String[] args) {
        SpringApplication.run(RunApplication.class, args);

    }
}